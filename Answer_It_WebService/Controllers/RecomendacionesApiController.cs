﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class RecomendacionesApiController : ApiController
    {
        // GET: api/RecomendacionesApi
        [HttpGet]
        public IQueryable<Recomendaciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Recomendaciones;

        }

        // GET: api/RecomendacionesApi/2
        [HttpGet]
        [ResponseType(typeof(Recomendaciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            Recomendaciones rec = await db.Recomendaciones.FindAsync(id);
            if (rec == null)
            {
                return NotFound();
            }
            return Ok(rec);

        }

        // POST: api/RecomendacionesApi
        [HttpPost]
        public bool Post(Recomendaciones rec) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Recomendaciones.Add(rec);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(Recomendaciones rec) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Recomendaciones rec_antigua = db.Recomendaciones.FirstOrDefault(x => x.id == rec.id);
            rec_antigua.id = rec.id;
            rec_antigua.contenido = rec.contenido;
            rec_antigua.id_usuario = rec.id_usuario;
            rec_antigua.id_solicitud_recomendacion = rec.id_solicitud_recomendacion;
            rec_antigua.me_gusta = rec.me_gusta;
            rec_antigua.eliminado = rec.eliminado;
            rec_antigua.fecha = rec.fecha;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            List<DenunciasRecomendaciones> denunciasABorrar = db.DenunciasRecomendaciones.Where(x => x.id_recomendacion == id).ToList();
            db.DenunciasRecomendaciones.RemoveRange(denunciasABorrar);
            List<ValoracionesMeGusta> valMeGustaABorrar = db.ValoracionesMeGusta.Where(x => x.id_valoracion == id).ToList();
            db.ValoracionesMeGusta.RemoveRange(valMeGustaABorrar);
            Recomendaciones rec_encontrada = db.Recomendaciones.FirstOrDefault(x => x.id == id);
            db.Recomendaciones.Remove(rec_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
