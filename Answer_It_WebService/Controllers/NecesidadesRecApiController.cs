﻿using Datos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Answer_It_WebService.Controllers
{
    public class NecesidadesRecApiController : ApiController
    {
        // GET: api/NecesidadesRecApi
        [HttpGet]
        public IQueryable<NecesidadesRec> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.NecesidadesRec;
        }

        // GET: api/NecesidadesRec/2
        [HttpGet]
        [ResponseType(typeof(NecesidadesRec))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            NecesidadesRec nec = await db.NecesidadesRec.Where(x => x.id_solicitud_recomendacion == id).FirstOrDefaultAsync();
            if (nec == null)
            {
                return NotFound();
            }
            return Ok(nec);

        }

        // POST: api/NecesidadesRecApi
        [HttpPost]
        public bool Post(NecesidadesRec nec) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.NecesidadesRec.Add(nec);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(NecesidadesRec nec) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NecesidadesRec nec_antigua = db.NecesidadesRec.FirstOrDefault(x => x.id_solicitud_recomendacion == nec.id_solicitud_recomendacion);
            //nec_antigua.id = nec.id; //No lo tengo, no lo actualizo!
            nec_antigua.nombre_necesidad = nec.nombre_necesidad;
            nec_antigua.id_solicitud_recomendacion = nec.id_solicitud_recomendacion;
            nec_antigua.es_necesidad_primaria = nec.es_necesidad_primaria;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id_sol_rec) //Elimino una necesidad
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NecesidadesRec nec_encontrada = db.NecesidadesRec.FirstOrDefault(x => x.id_solicitud_recomendacion == id_sol_rec);
            db.NecesidadesRec.Remove(nec_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
