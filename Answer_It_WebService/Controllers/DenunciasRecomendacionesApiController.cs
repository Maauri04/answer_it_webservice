﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class DenunciasRecomendacionesApiController : ApiController
    {
        // GET: api/DenunciasRecomendacionesApi
        [HttpGet]
        public IQueryable<DenunciasRecomendaciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.DenunciasRecomendaciones;

        }

        // GET: api/DenunciasRecomendacionesApi/2
        [HttpGet]
        [ResponseType(typeof(DenunciasRecomendaciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            DenunciasRecomendaciones denuncia = await db.DenunciasRecomendaciones.FindAsync(id);
            if (denuncia == null)
            {
                return NotFound();
            }
            return Ok(denuncia);

        }

        // POST: api/DenunciasRecomendacionesApi
        [HttpPost]
        public bool Post(DenunciasRecomendaciones denuncia) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.DenunciasRecomendaciones.Add(denuncia);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(DenunciasRecomendaciones denuncia) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            DenunciasRecomendaciones den_antigua = db.DenunciasRecomendaciones.FirstOrDefault(x => x.id == denuncia.id);
            den_antigua.id = denuncia.id;
            den_antigua.id_recomendacion = denuncia.id_recomendacion;
            den_antigua.id_usuario_denunciante = denuncia.id_usuario_denunciante;
            den_antigua.id_usuario_denunciado = denuncia.id_usuario_denunciado;
            den_antigua.motivo = denuncia.motivo;
            den_antigua.fecha_denuncia = denuncia.fecha_denuncia;
            den_antigua.reputacion_descontada = denuncia.reputacion_descontada;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            DenunciasRecomendaciones den_encontrada = db.DenunciasRecomendaciones.FirstOrDefault(x => x.id == id);
            db.DenunciasRecomendaciones.Remove(den_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
