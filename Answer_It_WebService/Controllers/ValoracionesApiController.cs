﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class ValoracionesApiController : ApiController
    {
        // GET: api/ValoracionesApi
        [HttpGet]
        public IQueryable<Valoraciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Valoraciones;

        }

        // GET: api/ValoracionesApi/2
        [HttpGet]
        [ResponseType(typeof(Valoraciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            Valoraciones val = await db.Valoraciones.FindAsync(id);
            if (val == null)
            {
                return NotFound();
            }
            return Ok(val);

        }

        // POST: api/ValoracionesApi
        [HttpPost]
        public bool Post(Valoraciones val) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Valoraciones.Add(val);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(Valoraciones val) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Valoraciones val_antigua = db.Valoraciones.FirstOrDefault(x => x.id == val.id);
            val_antigua.id = val.id;
            val_antigua.titulo = val.titulo;
            val_antigua.contenido = val.contenido;
            val_antigua.id_usuario = val.id_usuario;
            val_antigua.id_producto = val.id_producto;
            val_antigua.puntaje = val.puntaje;
            val_antigua.me_gusta = val.me_gusta;
            val_antigua.eliminado = val.eliminado;
            val_antigua.fecha = val.fecha;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino una valoracion
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            List<DenunciasValoraciones> denunciasABorrar = db.DenunciasValoraciones.Where(x => x.id_valoracion == id).ToList();
            db.DenunciasValoraciones.RemoveRange(denunciasABorrar);
            List<ValoracionesMeGusta> valMeGustaABorrar = db.ValoracionesMeGusta.Where(x => x.id_valoracion == id).ToList();
            db.ValoracionesMeGusta.RemoveRange(valMeGustaABorrar);
            Valoraciones val_encontrada = db.Valoraciones.FirstOrDefault(x => x.id == id);
            db.Valoraciones.Remove(val_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
