﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class ProductosApiController : ApiController
    {
        // GET: api/ProductosApi
        [HttpGet]
        public IQueryable<Productos> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Productos;

        }

        // GET: api/ProductosApi/2
        [HttpGet]
        [ResponseType(typeof(Productos))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            Productos prod = await db.Productos.FindAsync(id);
            if (prod == null)
            {
                return NotFound();
            }
            return Ok(prod);

        }

        // POST: api/ProductosApi
        [HttpPost]
        public int Post(Productos prod) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Productos.Add(prod);
            db.SaveChanges();
            return prod.id;
        }

        [HttpPut]
        public bool Put(Productos prod) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Productos prod_antiguo = db.Productos.FirstOrDefault(x => x.id == prod.id);
            prod_antiguo.id = prod.id;
            prod_antiguo.nombre = prod.nombre;
            prod_antiguo.valoracion = prod.valoracion;
            prod_antiguo.cant_votos = prod.cant_votos;
            prod_antiguo.fecha_creacion = prod.fecha_creacion;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Productos prod_encontrado = db.Productos.FirstOrDefault(x => x.id == id);
            db.Productos.Remove(prod_encontrado);
            return db.SaveChanges() > 0;
        }
    }
}
