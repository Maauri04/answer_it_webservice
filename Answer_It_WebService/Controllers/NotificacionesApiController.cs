﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class NotificacionesApiController : ApiController
    {
        // GET: api/RolesApi
        [HttpGet]
        public IQueryable<Notificaciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Notificaciones;

        }

        // GET: api/RolesApi/2
        [HttpGet]
        [ResponseType(typeof(Notificaciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            Notificaciones notificacion = await db.Notificaciones.FindAsync(id);
            if (notificacion == null)
            {
                return NotFound();
            }
            return Ok(notificacion);

        }

        // POST: api/RolesApi
        [HttpPost]
        public bool Post(Notificaciones not) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Notificaciones.Add(not);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(Notificaciones not) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Notificaciones not_antigua = db.Notificaciones.FirstOrDefault(x => x.id == not.id);
            not_antigua.id_usuario_origen = not.id_usuario_origen;
            not_antigua.id_usuario_destino = not.id_usuario_destino;
            not_antigua.descripcion = not.descripcion;
            not_antigua.leida = not.leida;
            not_antigua.id_publicacion = not.id;
            not_antigua.fecha = not.fecha;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Notificaciones not_encontrada = db.Notificaciones.FirstOrDefault(x => x.id == id);
            db.Notificaciones.Remove(not_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
