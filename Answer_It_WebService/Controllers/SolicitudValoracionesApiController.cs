﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class SolicitudValoracionesApiController : ApiController
    {
        // GET: api/SolicitudValoracionesApi
        [HttpGet]
        public IQueryable<SolicitudValoraciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.SolicitudValoraciones;

        }

        // GET: api/RolesApi/2
        [HttpGet]
        [ResponseType(typeof(SolicitudValoraciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            SolicitudValoraciones sol = await db.SolicitudValoraciones.FindAsync(id);
            if (sol == null)
            {
                return NotFound();
            }
            return Ok(sol);

        }

        // POST: api/RolesApi
        [HttpPost]
        public bool Post(SolicitudValoraciones sol) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.SolicitudValoraciones.Add(sol);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(SolicitudValoraciones sol) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            SolicitudValoraciones sol_antigua = db.SolicitudValoraciones.FirstOrDefault(x => x.id == sol.id);
            sol_antigua.id = sol.id;
            sol_antigua.leyenda = sol.leyenda;
            sol_antigua.id_usuario = sol.id_usuario;
            sol_antigua.visitas = sol.visitas;
            sol_antigua.eliminado = sol.eliminado;
            sol_antigua.fecha = sol.fecha;
            sol_antigua.cant_recomendaciones = sol.cant_recomendaciones;
            sol_antigua.reputacion_otorgada = sol.reputacion_otorgada;
            sol_antigua.sin_skill = sol.sin_skill;
            sol_antigua.skill_prisa = sol.skill_prisa;
            sol_antigua.skill_destacado = sol.skill_destacado;
            sol_antigua.id_producto = sol.id_producto;
            sol_antigua.aspecto_especial = sol.aspecto_especial;
            sol_antigua.r_points_recompensa = sol.r_points_recompensa;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            SolicitudValoraciones sol_encontrada = db.SolicitudValoraciones.FirstOrDefault(x => x.id == id);
            db.SolicitudValoraciones.Remove(sol_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
