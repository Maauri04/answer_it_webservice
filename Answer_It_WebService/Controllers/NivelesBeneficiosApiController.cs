﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Answer_It_WebService.Controllers
{
    public class NivelesBeneficiosApiController : ApiController
    {
        // GET: api/DenunciasApi
        [HttpGet]
        public IQueryable<NivelesBeneficios> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.NivelesBeneficios;

        }

        // GET: api/DenunciasApi/2
        [HttpGet]
        [ResponseType(typeof(NivelesBeneficios))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            NivelesBeneficios nivelBeneficio = await db.NivelesBeneficios.FindAsync(id);
            if (nivelBeneficio == null)
            {
                return NotFound();
            }
            return Ok(nivelBeneficio);

        }

        // POST: api/DenunciasApi
        [HttpPost]
        public bool Post(NivelesBeneficios nivelBeneficio) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.NivelesBeneficios.Add(nivelBeneficio);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(NivelesBeneficios nivelBeneficio) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NivelesBeneficios nivelBeneficio_antiguo = db.NivelesBeneficios.FirstOrDefault(x => x.id == nivelBeneficio.id);
            nivelBeneficio_antiguo.id = nivelBeneficio.id;
            nivelBeneficio_antiguo.nivel = nivelBeneficio.nivel;
            nivelBeneficio_antiguo.reputacion_necesaria = nivelBeneficio.reputacion_necesaria;
            nivelBeneficio_antiguo.beneficios = nivelBeneficio.beneficios;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NivelesBeneficios niv_encontrado = db.NivelesBeneficios.FirstOrDefault(x => x.id == id);
            db.NivelesBeneficios.Remove(niv_encontrado);
            return db.SaveChanges() > 0;
        }
    }
}
