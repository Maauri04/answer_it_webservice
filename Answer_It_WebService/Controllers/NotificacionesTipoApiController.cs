﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class NotificacionesTipoApiController : ApiController
    {
        // GET: api/RolesApi
        [HttpGet]
        public IQueryable<NotificacionesTipo> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.NotificacionesTipo;

        }

        // GET: api/RolesApi/2
        [HttpGet]
        [ResponseType(typeof(NotificacionesTipo))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            NotificacionesTipo notificacion_tipo = await db.NotificacionesTipo.FindAsync(id);
            if (notificacion_tipo == null)
            {
                return NotFound();
            }
            return Ok(notificacion_tipo);

        }

        // POST: api/RolesApi
        [HttpPost]
        public bool Post(NotificacionesTipo not_tipo) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.NotificacionesTipo.Add(not_tipo);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(NotificacionesTipo not_tipo) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NotificacionesTipo not_antigua = db.NotificacionesTipo.FirstOrDefault(x => x.id == not_tipo.id);
            not_antigua.id = not_tipo.id;
            not_antigua.tipo = not_tipo.tipo;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            NotificacionesTipo not_encontrada = db.NotificacionesTipo.FirstOrDefault(x => x.id == id);
            db.NotificacionesTipo.Remove(not_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
