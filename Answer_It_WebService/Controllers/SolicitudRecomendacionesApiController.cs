﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class SolicitudRecomendacionesApiController : ApiController
    {
        // GET: api/SolicitudRecomendacionesApi
        [HttpGet]
        public IQueryable<SolicitudRecomendaciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.SolicitudRecomendaciones;

        }

        // GET: api/RolesApi/2
        [HttpGet]
        [ResponseType(typeof(SolicitudRecomendaciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            SolicitudRecomendaciones sol = await db.SolicitudRecomendaciones.FindAsync(id);
            if (sol == null)
            {
                return NotFound();
            }
            return Ok(sol);

        }

        // POST: api/RolesApi
        [HttpPost]
        public int Post(SolicitudRecomendaciones sol) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.SolicitudRecomendaciones.Add(sol);
            db.SaveChanges();
            return sol.id;
        }

        [HttpPut]
        public bool Put(SolicitudRecomendaciones sol) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            SolicitudRecomendaciones sol_antigua = db.SolicitudRecomendaciones.FirstOrDefault(x => x.id == sol.id);
            sol_antigua.id = sol.id;
            sol_antigua.titulo = sol.titulo;
            sol_antigua.leyenda = sol.leyenda;
            sol_antigua.id_usuario = sol.id_usuario;
            sol_antigua.visitas = sol.visitas;
            sol_antigua.eliminado = sol.eliminado;
            sol_antigua.fecha = sol.fecha;
            sol_antigua.reputacion_otorgada = sol.reputacion_otorgada;
            sol_antigua.sin_skill = sol.sin_skill;
            sol_antigua.skill_prisa = sol.skill_prisa;
            sol_antigua.skill_destacado = sol.skill_destacado;
            sol_antigua.nivel_requerido = sol.nivel_requerido;
            sol_antigua.contenido = sol.contenido;
            sol_antigua.tipo_producto = sol.tipo_producto;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id)
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            List<NecesidadesRec> necesidadesABorrar = db.NecesidadesRec.Where(x => x.id_solicitud_recomendacion == id).ToList();
            db.NecesidadesRec.RemoveRange(necesidadesABorrar);
            List<Recomendaciones> recomendacionesABorrar = db.Recomendaciones.Where(x => x.id_solicitud_recomendacion == id).ToList();
            db.Recomendaciones.RemoveRange(recomendacionesABorrar);
            SolicitudRecomendaciones sol_encontrada = db.SolicitudRecomendaciones.FirstOrDefault(x => x.id == id);
            db.SolicitudRecomendaciones.Remove(sol_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
