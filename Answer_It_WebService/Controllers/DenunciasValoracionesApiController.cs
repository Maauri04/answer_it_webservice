﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class DenunciasValoracionesApiController : ApiController
    {
        // GET: api/DenunciasValoracionesApi
        [HttpGet]
        public IQueryable<DenunciasValoraciones> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.DenunciasValoraciones;

        }

        // GET: api/DenunciasValoracionesApi/2
        [HttpGet]
        [ResponseType(typeof(DenunciasValoraciones))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            DenunciasValoraciones denuncia = await db.DenunciasValoraciones.FindAsync(id);
            if (denuncia == null)
            {
                return NotFound();
            }
            return Ok(denuncia);

        }

        // POST: api/DenunciasValoracionesApi
        [HttpPost]
        public bool Post(DenunciasValoraciones denuncia) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.DenunciasValoraciones.Add(denuncia);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(DenunciasValoraciones denuncia) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            DenunciasValoraciones den_antigua = db.DenunciasValoraciones.FirstOrDefault(x => x.id == denuncia.id);
            den_antigua.id = denuncia.id;
            den_antigua.id_valoracion = denuncia.id_valoracion;
            den_antigua.id_usuario_denunciante = denuncia.id_usuario_denunciante;
            den_antigua.id_usuario_denunciado = denuncia.id_usuario_denunciado;
            den_antigua.motivo = denuncia.motivo;
            den_antigua.fecha_denuncia = denuncia.fecha_denuncia;
            den_antigua.reputacion_descontada = denuncia.reputacion_descontada;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            DenunciasValoraciones den_encontrada = db.DenunciasValoraciones.FirstOrDefault(x => x.id == id);
            db.DenunciasValoraciones.Remove(den_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
