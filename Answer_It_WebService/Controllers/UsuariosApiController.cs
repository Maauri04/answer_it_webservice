﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class UsuariosApiController : ApiController
    {
        // GET: api/UsuariosApi
        [HttpGet]
        public IQueryable<Usuarios> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Usuarios;

        }

        // GET: api/UsuariosApi/2
        [HttpGet]
        [ResponseType(typeof(Usuarios))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            Usuarios usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            return Ok(usuario);

        }

        // POST: api/UsuariosApi
        [HttpPost]
        public int Post(Usuarios usuario) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Usuarios.Add(usuario);
            db.SaveChanges();
            return usuario.id;
        }

        [HttpPut]
        public bool Put(Usuarios usuario) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            Usuarios usu_antiguo = db.Usuarios.FirstOrDefault(x => x.id == usuario.id);

            usu_antiguo.id = usuario.id;
            usu_antiguo.email = usuario.email;
            usu_antiguo.nombre_usuario = usuario.nombre_usuario;
            usu_antiguo.contrasena = usu_antiguo.contrasena;
            usu_antiguo.apellido = usuario.apellido;
            usu_antiguo.nombre = usuario.nombre;
            usu_antiguo.id_rol = usuario.id_rol;
            usu_antiguo.pais = usuario.pais;
            usu_antiguo.localidad = usuario.localidad;
            usu_antiguo.reputacion = usuario.reputacion;
            usu_antiguo.nivel = usuario.nivel;
            usu_antiguo.eliminado = usuario.eliminado;
            usu_antiguo.fecha_registro = usuario.fecha_registro;
            usu_antiguo.r_points = usuario.r_points;
            usu_antiguo.visitas_al_perfil = usuario.visitas_al_perfil;
            usu_antiguo.votos_positivos = usuario.votos_positivos;
            usu_antiguo.votos_negativos = usuario.votos_negativos;
            usu_antiguo.es_verificado = usuario.es_verificado;
            usu_antiguo.edad = usuario.edad;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            List<ImagenesUsuarios> imagenesABorrar = db.ImagenesUsuarios.Where(x => x.id_usuario == id).ToList();
            db.ImagenesUsuarios.RemoveRange(imagenesABorrar);
            List<Notificaciones> notificacionesABorrar = db.Notificaciones.Where(x => x.id_usuario_origen == id || x.id_usuario_destino == id).ToList();
            db.Notificaciones.RemoveRange(notificacionesABorrar);
            List<SolicitudRecomendaciones> solicitudesRecABorrar = db.SolicitudRecomendaciones.Where(x => x.id_usuario == id).ToList();
            foreach (var item in solicitudesRecABorrar)
            {
                List<NecesidadesRec> necesidadesABorrar = db.NecesidadesRec.Where(x => x.id_solicitud_recomendacion == item.id).ToList();
                db.NecesidadesRec.RemoveRange(necesidadesABorrar);
                List<Recomendaciones> recomendacionesABorrar = db.Recomendaciones.Where(x => x.id_solicitud_recomendacion == item.id).ToList();
                foreach (var rec in recomendacionesABorrar)
                {
                    List<DenunciasRecomendaciones> denunciasRecABorrar = db.DenunciasRecomendaciones.Where(x => x.id_recomendacion == rec.id).ToList();
                    db.DenunciasRecomendaciones.RemoveRange(denunciasRecABorrar);
                }
                db.Recomendaciones.RemoveRange(recomendacionesABorrar);
            }      
            db.SolicitudRecomendaciones.RemoveRange(solicitudesRecABorrar);
            List<SolicitudValoraciones> solicitudesValABorrar = db.SolicitudValoraciones.Where(x => x.id_usuario == id).ToList();
            db.SolicitudValoraciones.RemoveRange(solicitudesValABorrar);

            List<Valoraciones> valsABorrar = db.Valoraciones.Where(x => x.id_usuario == id).ToList();
            foreach (var val in valsABorrar)
            {
                List<DenunciasValoraciones> denunciasValABorrar = db.DenunciasValoraciones.Where(x => x.id_valoracion == val.id).ToList();
                db.DenunciasValoraciones.RemoveRange(denunciasValABorrar);
                List<ValoracionesMeGusta> valMeGustaABorrar = db.ValoracionesMeGusta.Where(x => x.id_valoracion == val.id).ToList();
                db.ValoracionesMeGusta.RemoveRange(valMeGustaABorrar);
            }
            db.Valoraciones.RemoveRange(valsABorrar);
            Usuarios usu_encontrado = db.Usuarios.FirstOrDefault(x => x.id == id);
            db.Usuarios.Remove(usu_encontrado);
            return db.SaveChanges() > 0; 
        }
    }
}
