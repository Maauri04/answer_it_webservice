﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;

namespace Answer_It_WebService.Controllers
{
    public class ImagenesUsuariosApiController : ApiController
    {
        // GET: api/ImagenesUsuariosApi
        [HttpGet]
        public IQueryable<ImagenesUsuarios> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.ImagenesUsuarios;

        }

        // GET: api/ImagenesUsuarios/2
        [HttpGet]
        [ResponseType(typeof(ImagenesUsuarios))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            ImagenesUsuarios img = await db.ImagenesUsuarios.Where(x => x.id_usuario == id).FirstOrDefaultAsync(); //BUSCAR POR id_usuario!!!
            if (img == null)
            {
                return NotFound();
            }
            return Ok(img);

        }

        // POST: api/ImagenesUsuariosApi
        [HttpPost]
        public bool Post(ImagenesUsuarios img) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.ImagenesUsuarios.Add(img);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(ImagenesUsuarios img) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ImagenesUsuarios img_antigua = db.ImagenesUsuarios.FirstOrDefault(x => x.id_usuario == img.id_usuario);
            img_antigua.nombre_imagen = img.nombre_imagen;
            img_antigua.id_usuario = img.id_usuario;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ImagenesUsuarios img_encontrada = db.ImagenesUsuarios.FirstOrDefault(x => x.id_usuario == id);
            db.ImagenesUsuarios.Remove(img_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
