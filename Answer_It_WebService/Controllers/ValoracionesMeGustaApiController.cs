﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace Answer_It_WebService.Controllers
{
    public class ValoracionesMeGustaApiController : ApiController
    {
        // GET: api/ValoracionesMeGustaApi
        [HttpGet]
        public IQueryable<ValoracionesMeGusta> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.ValoracionesMeGusta;

        }

        // GET: api/ValoracionesMeGustaApi/2
        [HttpGet]
        [ResponseType(typeof(ValoracionesMeGusta))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            ValoracionesMeGusta val = await db.ValoracionesMeGusta.FindAsync(id);
            if (val == null)
            {
                return NotFound();
            }
            return Ok(val);

        }

        // POST: api/ValoracionesApi
        [HttpPost]
        public bool Post(ValoracionesMeGusta val) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.ValoracionesMeGusta.Add(val);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(ValoracionesMeGusta val) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ValoracionesMeGusta val_antigua = db.ValoracionesMeGusta.FirstOrDefault(x => x.id == val.id);
            val_antigua.id = val.id;
            val_antigua.id_valoracion = val.id_valoracion;
            val_antigua.id_usuario_like = val.id_usuario_like;
            val_antigua.me_gusta = val.me_gusta;
            val_antigua.no_me_gusta = val.no_me_gusta;

            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ValoracionesMeGusta val_encontrada = db.ValoracionesMeGusta.FirstOrDefault(x => x.id == id);
            db.ValoracionesMeGusta.Remove(val_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
