﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;

namespace Answer_It_WebService.Controllers
{
    public class ImagenesProductosApiController : ApiController
    {
        // GET: api/ImagenesProductosApi
        [HttpGet]
        public IQueryable<ImagenesProductos> Get()
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.ImagenesProductos;
        }

        // GET: api/ImagenesProductos/2
        [HttpGet]
        [ResponseType(typeof(ImagenesProductos))]
        public async Task<IHttpActionResult> Get(int id)
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            ImagenesProductos img = await db.ImagenesProductos.Where(x => x.id_producto == id).FirstOrDefaultAsync();
            if (img == null)
            {
                return NotFound();
            }
            return Ok(img);

        }

        // POST: api/ImagenesProductosApi
        [HttpPost]
        public bool Post(ImagenesProductos img) //Agrego un registro
        {
            RecommendItEntities db = new RecommendItEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.ImagenesProductos.Add(img);
            return db.SaveChanges() > 0;
        }

        [HttpPut]
        public bool Put(ImagenesProductos img) //Modifico un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ImagenesProductos img_antigua = db.ImagenesProductos.FirstOrDefault(x => x.id_producto == img.id_producto);
            //img_antigua.id = img.id; //No lo tengo, no lo actualizo!
            img_antigua.nombre_imagen = img.nombre_imagen;
            img_antigua.id_producto = img.id_producto;
            return db.SaveChanges() > 0;
        }

        [HttpDelete]
        public bool Delete(int id) //Elimino un registro
        {
            RecommendItEntities db = new RecommendItEntities();

            db.Configuration.ProxyCreationEnabled = false;
            ImagenesProductos img_encontrada = db.ImagenesProductos.FirstOrDefault(x => x.id_producto == id);
            db.ImagenesProductos.Remove(img_encontrada);
            return db.SaveChanges() > 0;
        }
    }
}
