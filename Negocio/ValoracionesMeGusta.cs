﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class ValoracionesMeGusta
    {
        public int id { get; set; }
        public int id_valoracion { get; set; }
        public int id_usuario_like { get; set; }
        public bool me_gusta { get; set; }
        public bool no_me_gusta { get; set; }
    }
}
