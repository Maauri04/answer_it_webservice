﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Notificaciones
    {
        public int Id { get; set; }
        public int IdUsuarioOrigen { get; set; }
        public int IdUsuarioDestino { get; set; }
        public string Descripcion { get; set; }
        public bool Leida { get; set; }
        public int IdTipo { get; set; }
        public int? IdPublicacion { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
