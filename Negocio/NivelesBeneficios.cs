﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class NivelesBeneficios
    {
        public int id { get; set; }
        public int nivel { get; set; }
        public int reputacion_necesaria { get; set; }
        public string beneficios { get; set; }
    }
}
