﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class DenunciasValoraciones
    {
        public int id { get; set; }
        public int id_valoracion { get; set; }
        public int id_usuario_denunciante { get; set; }
        public int id_usuario_denunciado { get; set; }
        public string motivo { get; set; }
        public DateTime? fecha_denuncia { get; set; }
    }
}
