﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Productos
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public decimal? valoracion { get; set; }
        public int? cant_votos { get; set; }
        public DateTime? fecha_creacion { get; set; }
    }
}
