﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Denuncias
    {
        public int id { get; set; }
        public int motivo { get; set; }
        public int? reputacion_descontada { get; set; }
    }
}
