USE [master]
GO
/****** Object:  Database [RecommendIt]    Script Date: 2/11/2022 21:52:04 ******/
CREATE DATABASE [RecommendIt]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RecommendIt', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\RecommendIt.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RecommendIt_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\RecommendIt_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [RecommendIt] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RecommendIt].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RecommendIt] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RecommendIt] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RecommendIt] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RecommendIt] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RecommendIt] SET ARITHABORT OFF 
GO
ALTER DATABASE [RecommendIt] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RecommendIt] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RecommendIt] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RecommendIt] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RecommendIt] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RecommendIt] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RecommendIt] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RecommendIt] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RecommendIt] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RecommendIt] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RecommendIt] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RecommendIt] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RecommendIt] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RecommendIt] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RecommendIt] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RecommendIt] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RecommendIt] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RecommendIt] SET RECOVERY FULL 
GO
ALTER DATABASE [RecommendIt] SET  MULTI_USER 
GO
ALTER DATABASE [RecommendIt] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RecommendIt] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RecommendIt] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RecommendIt] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RecommendIt] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'RecommendIt', N'ON'
GO
ALTER DATABASE [RecommendIt] SET QUERY_STORE = OFF
GO
USE [RecommendIt]
GO
/****** Object:  Table [dbo].[DenunciasRecomendaciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DenunciasRecomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_recomendacion] [int] NOT NULL,
	[id_usuario_denunciante] [int] NOT NULL,
	[id_usuario_denunciado] [int] NOT NULL,
	[motivo] [int] NOT NULL,
	[fecha_denuncia] [datetime] NULL,
	[reputacion_descontada] [int] NULL,
 CONSTRAINT [PK_DenunciasRecomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_recomendacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DenunciasValoraciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DenunciasValoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_valoracion] [int] NOT NULL,
	[id_usuario_denunciante] [int] NOT NULL,
	[id_usuario_denunciado] [int] NOT NULL,
	[motivo] [int] NOT NULL,
	[fecha_denuncia] [datetime] NULL,
	[reputacion_descontada] [int] NULL,
 CONSTRAINT [PK_DenunciasValoraciones_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NivelesBeneficios]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelesBeneficios](
	[id] [int] NOT NULL,
	[nivel] [int] NOT NULL,
	[reputacion_necesaria] [int] NOT NULL,
	[beneficios] [nvarchar](300) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notificaciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notificaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario_origen] [int] NOT NULL,
	[id_usuario_destino] [int] NOT NULL,
	[descripcion] [nvarchar](200) NOT NULL,
	[leida] [bit] NOT NULL,
	[id_tipo] [int] NOT NULL,
	[id_publicacion] [int] NULL,
	[fecha] [datetime] NULL,
 CONSTRAINT [PK_Notificaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificacionesTipo]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificacionesTipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_NotificacionesTipo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NOT NULL,
	[foto] [nvarchar](max) NULL,
	[valoracion] [decimal](2, 0) NULL,
	[cant_votos] [int] NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recomendaciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contenido] [nvarchar](max) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_solicitud_recomendacion] [int] NOT NULL,
	[me_gusta] [int] NOT NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
 CONSTRAINT [PK_Recomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SolicitudRecomendaciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitudRecomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](max) NOT NULL,
	[leyenda] [nvarchar](256) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[visitas] [int] NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[reputacion_otorgada] [int] NOT NULL,
	[sin_skill] [bit] NOT NULL,
	[skill_prisa] [bit] NOT NULL,
	[skill_destacado] [bit] NOT NULL,
	[nivel_requerido] [int] NULL,
	[contenido] [nvarchar](max) NULL,
	[tipo_producto] [nvarchar](300) NULL,
 CONSTRAINT [PK_SolicitudRecomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SolicitudValoraciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitudValoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[leyenda] [nvarchar](256) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[visitas] [int] NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[cant_recomendaciones] [int] NOT NULL,
	[reputacion_otorgada] [int] NOT NULL,
	[sin_skill] [bit] NOT NULL,
	[skill_prisa] [bit] NOT NULL,
	[skill_destacado] [bit] NOT NULL,
	[id_producto] [int] NOT NULL,
 CONSTRAINT [PK_Preguntas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](256) NULL,
	[nombre_usuario] [nvarchar](256) NOT NULL,
	[contrasena] [nvarchar](256) NOT NULL,
	[apellido] [nvarchar](max) NOT NULL,
	[nombre] [nvarchar](256) NOT NULL,
	[id_rol] [int] NOT NULL,
	[pais] [nvarchar](256) NULL,
	[localidad] [nvarchar](256) NULL,
	[reputacion] [int] NOT NULL,
	[nivel] [int] NOT NULL,
	[foto] [nvarchar](max) NULL,
	[eliminado] [bit] NOT NULL,
	[fecha_registro] [datetime] NULL,
	[r_points] [int] NULL,
	[visitas_al_perfil] [int] NULL,
	[votos_positivos] [int] NULL,
	[votos_negativos] [int] NULL,
	[es_verificado] [bit] NULL,
	[edad] [int] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Valoraciones]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Valoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](100) NOT NULL,
	[contenido] [nvarchar](max) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_producto] [int] NOT NULL,
	[puntaje] [int] NOT NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[me_gusta] [int] NOT NULL,
 CONSTRAINT [PK_Valoraciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValoracionesMeGusta]    Script Date: 2/11/2022 21:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValoracionesMeGusta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_valoracion] [int] NOT NULL,
	[id_usuario_like] [int] NOT NULL,
	[me_gusta] [bit] NOT NULL,
	[no_me_gusta] [bit] NOT NULL,
 CONSTRAINT [PK_ValoracionesMeGusta_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Recomendaciones] FOREIGN KEY([id_recomendacion])
REFERENCES [dbo].[Recomendaciones] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Recomendaciones]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios1] FOREIGN KEY([id_usuario_denunciante])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios1]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios2] FOREIGN KEY([id_usuario_denunciado])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios2]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Usuarios1] FOREIGN KEY([id_usuario_denunciante])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Usuarios1]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Usuarios2] FOREIGN KEY([id_usuario_denunciado])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Usuarios2]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Valoraciones] FOREIGN KEY([id_valoracion])
REFERENCES [dbo].[Valoraciones] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Valoraciones]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_NotificacionesTipo] FOREIGN KEY([id_tipo])
REFERENCES [dbo].[NotificacionesTipo] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_NotificacionesTipo]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_Usuarios] FOREIGN KEY([id_usuario_origen])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_Usuarios]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_Usuarios2] FOREIGN KEY([id_usuario_destino])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_Usuarios2]
GO
ALTER TABLE [dbo].[Recomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_Recomendaciones_SolicitudRecomendaciones] FOREIGN KEY([id_solicitud_recomendacion])
REFERENCES [dbo].[SolicitudRecomendaciones] ([id])
GO
ALTER TABLE [dbo].[Recomendaciones] CHECK CONSTRAINT [FK_Recomendaciones_SolicitudRecomendaciones]
GO
ALTER TABLE [dbo].[Recomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_Recomendaciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Recomendaciones] CHECK CONSTRAINT [FK_Recomendaciones_Usuarios]
GO
ALTER TABLE [dbo].[SolicitudRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudRecomendaciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[SolicitudRecomendaciones] CHECK CONSTRAINT [FK_SolicitudRecomendaciones_Usuarios]
GO
ALTER TABLE [dbo].[SolicitudValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudValoraciones_Productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[SolicitudValoraciones] CHECK CONSTRAINT [FK_SolicitudValoraciones_Productos]
GO
ALTER TABLE [dbo].[SolicitudValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudValoraciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[SolicitudValoraciones] CHECK CONSTRAINT [FK_SolicitudValoraciones_Usuarios]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[Roles] ([id])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Roles]
GO
ALTER TABLE [dbo].[Valoraciones]  WITH CHECK ADD  CONSTRAINT [FK_Valoraciones_Productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[Valoraciones] CHECK CONSTRAINT [FK_Valoraciones_Productos]
GO
ALTER TABLE [dbo].[Valoraciones]  WITH CHECK ADD  CONSTRAINT [FK_Valoraciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Valoraciones] CHECK CONSTRAINT [FK_Valoraciones_Usuarios]
GO
ALTER TABLE [dbo].[ValoracionesMeGusta]  WITH CHECK ADD  CONSTRAINT [FK_ValoracionesMeGusta_Usuarios] FOREIGN KEY([id_usuario_like])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[ValoracionesMeGusta] CHECK CONSTRAINT [FK_ValoracionesMeGusta_Usuarios]
GO
ALTER TABLE [dbo].[ValoracionesMeGusta]  WITH CHECK ADD  CONSTRAINT [FK_ValoracionesMeGusta_Valoraciones] FOREIGN KEY([id_valoracion])
REFERENCES [dbo].[Valoraciones] ([id])
GO
ALTER TABLE [dbo].[ValoracionesMeGusta] CHECK CONSTRAINT [FK_ValoracionesMeGusta_Valoraciones]
GO
USE [master]
GO
ALTER DATABASE [RecommendIt] SET  READ_WRITE 
GO
